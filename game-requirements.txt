### Game Requirements ###

1. It consists of a grid of 16 cards.
2. The game knows how to manage matched and unmatched cards.
3. The game displays the current number of moves.
4. When the player starts a game, a displayed timer also starts.
5. If the player wins the game, the timer stops.
6. A congratulations modal appears when the player wins.
7. It shows a button to play again and the modal shows: how much time it took & how many    moves were involved.