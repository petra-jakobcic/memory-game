/* ---- Write the "engine" code to model the system ---- */

(function() {
    let state = 1;
    const cards = document.getElementsByClassName('game__card');
    
    Array.prototype.forEach.call(cards, function(card) {
        card.addEventListener('click', function(e) {
            e.preventDefault();

            switch(state) {
                case 0:
                    this.classList.add('game__card--showAnimal');
                    state = 1;
                    break;
                case 1:
                    if (this.classList.contains('game__card--showAnimal')) {
                        this.classList.remove('game__card--showAnimal');
                        state = 0;
                    } else {
                        this.classList.add('game__card--showAnimal');

                        let shownCards = document.querySelectorAll('.game__card--showAnimal');

                        let src1 = shownCards[0].children[0].src
                        let src2 = shownCards[1].children[0].src

                        console.log("Source1 is " + src1);
                        console.log("Source2 is " + src2);

                        if (src1 === src2) {
                            Array.prototype.forEach.call(shownCards, function(card) {
                                card.style.visibility = "hidden";
                                card.classList.remove('game__card--showAnimal');
                            });

                            state = 0;
                        } else {
                            state = 2;
                        }
                    }
                    break;
                case 2:
                    if (this.classList.contains('game__card--showAnimal')) {
                        this.classList.remove('game__card--showAnimal');
                        state = 1;
                    }
                    break;
                default:
                    console.error('Unexpected state: ' + state);
            }

            console.log('We are now State: ' + state);
        });
    });
}());


/* ---- Get the HTML elements we need ---- */



/* ---- Write the functions we need ---- */

//setUpTheGame();




/* ---- Tie everything together ---- */




/* ---- Rules of the game ---- */

// I start the game by clicking on one card.
// That card is revealed.
// It stays open.
// If I want to close that card, I click on it again.
// Then I click on the second card.
// The second card is revealed.
// It stays open.
// The board is locked if any two cards are revealed at the same time.
// The cards are then compared.
// If they match, they disappear.
// If they don't match, I keep playing.

/* ---- Functions to write ---- */

// When I click on a card, it is revealed.
// If I click on a revealed card, it closes.
// You can reveal two cards at the same time.
// If two cards are revealed at the same time, the board gets locked.
// The two revealed cards are compared.
// If they match, they disappear after 3 seconds.
// The board is unlocked after those 3 seconds.
// If they don't match, nothing happens, but the board is still locked.
// The board is unlocked if only 1 or none of the cards are revealed.

let boardIsLocked = false;
let numberOfVisibleCards = 0;

/**
 * Sets up the game.
 * 
 * @returns void
 */
function setUpTheGame() {
    const allCards = getAllCards();
    assignEventListenerToCards(allCards);
}

/**
 * Selects all the cards.
 * 
 * @returns NodeList All the cards.
 */
function getAllCards() {
    return document.querySelectorAll(".game__card");
}

/**
 * Assigns an event listener to the given cards.
 * 
 * @param {NodeList} cards a nodelist of cards
 * @returns void
 */
function assignEventListenerToCards(cards) {
    for (let i = 0; i < cards.length; i++) {
        assignEventListenerToOneCard(cards[i]);
    }
}

/**
 * Assigns an event listener to one card.
 * 
 * @param {HTMLElement} card a card element
 * @returns void
 */
function assignEventListenerToOneCard(card) {
    card.addEventListener("click", clickOnCard);
}

/**
 * Decides what happens when you click on a card.
 * 
 * @returns void
 */
function clickOnCard() {
    // if (boardIsLocked) {
    //     return;
    // }

    if (theAnimalIsHidden(this)) {
        showTheAnimal(this);

        // if (twoAnimalsAreRevealed()) {
        //     lockBoard();

        //     if (animalsAreEqual()) {
        //         makeMatchingCardsDisappear();
        //     } else {
        //         hideAllAnimals();
        //     }
        // }
    } else {
        hideTheAnimal(this);
    }
}

/**
 * Checks if a given card is hidden.
 * 
 * @returns boolean Whether or not a card is hidden.
 */
function theAnimalIsHidden(card) {
    if (card.classList.contains("game__card--showAnimal")) {
        return false;
    } else {
        return true;
    }
}

/**
 * Reveals the card that was clicked.
 * 
 * @param {HTMLElement} cardToReveal a card element
 * @returns void
 */
function showTheAnimal(cardToReveal) {
    cardToReveal.classList.add("game__card--showAnimal");
    // numberOfVisibleCards++;
}

/**
 * Checks if there are 2 cards revealed at the same time.
 * 
 * @returns boolean Whether or not 2 cards are revealed at the same time.
 */
function twoAnimalsAreRevealed() {
    // numberOfVisibleCards === 2;

    let cardsShowingTheAnimal = document.querySelectorAll(".game__card--showAnimal");

    if (cardsShowingTheAnimal.length === 2) {
        return true;
    } else {
        return false;
    }
}

/**
 * Locks the game board if two cards are revealed at the same time.
 * 
 * @returns void
 */
function lockBoard() {
    if (twoAnimalsAreRevealed()) {
        boardIsLocked = true;
//         // removeEventListenerFromCardsNotShowingTheAnimal();
    }
}

/**
 * Removes the event listener from the cards which are not visible.
 * 
 * @returns void
 */
// function removeEventListenerFromCardsNotShowingTheAnimal(cardsNotShowingTheAnimal) {
//     cardsNotShowingTheAnimal = getCardsNotShowingTheAnimal();

//     for (let i = 0; i < cardsNotShowingTheAnimal; i++) {
//         cardsNotShowingTheAnimal[i].removeEventListener("click", clickOnCard);
//     }
// }

/**
 * Gets all the cards not showing the animal.
 * 
 * @returns Nodelist All cards not showing the animal.
 * 
 */
// function getCardsNotShowingTheAnimal() {
//     return document.querySelectorAll("div.game__card:not(.game__card--showAnimal)");
// }

/**
 * Checks if two cards are the same.
 * 
 * @returns boolean Whether or not the cards are the same.
 */
// function animalsAreEqual() {
//     const revealedAnimals = document.querySelectorAll(".game__card--showAnimal");

//     if (revealedAnimals.length === 2) {
//         const animal1 = revealedAnimals[0];
//         const animal2 = revealedAnimals[1];

//         const back1 = animal1.children[0];
//         const back2 = animal2.children[0];

//         return back1.src === back2.src;
//     } else {
//         return false;
//     }
// }

/**
 * Makes the matching cards disappear after 3 seconds.
 * 
 * @param {HTMLElement} matchingCard1 a card element
 * @param {HTMLElement} matchingCard2 a card element
 * 
 * @returns void
 */
// function makeMatchingCardsDisappear(matchingCard1, matchingCard2) {
//     after 3 seconds matchingCard1 and matchingCard2 disappear;
// }

/**
 * Makes the unmatching cards disappear after 3 seconds.
 * 
 * @param {HTMLElement} unmatchingCard1 a card element
 * @param {HTMLElement} unmatchingCard2 a card element
 * 
 * @returns void
 */
// function hideAllAnimals(unmatchingCard1, unmatchingCard2) {
//     after 3 seconds unmatchingCard1 and unmatchingCard2 disappear;
// }

/**
 * Closes the revealed card.
 * 
 * @param {HTMLElement} cardToClose a card element
 * @returns void
 */
function hideTheAnimal(card) {
    card.classList.remove("game__card--showAnimal");
    //numberOfVisibleCards--;
}

/**
 * Checks if the board is locked.
 * 
 * @return boolean Whether or not the board is locked.
 */
// function isBoardLocked() {
//     if (boardIsLocked) {
//         return true;
//     } else {
//         return false;
//     }
// }
